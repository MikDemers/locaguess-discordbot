# LocaGuess-DiscordBot

## Introduction

Bienvenue sur LocaGuess-DiscordBot

Le projet est encore  en pleine réalisation comme le README. 

## Ajouter le bot à un serveur discord

Pour tester le bot, vous avez besoin d'avoir un serveur discord. Je vous invite à consulter le site que j'ai utilisé pour coder le bot: https://discordnet.dev/guides/getting_started/first-bot.html. Les explications sont assez précises. Vous remplacez mes informations par vos informations, aussi simple que ça!

## Obtention du token pour le jeu

1. Pour pouvoir utiliser l'api du site MapBox (ce site fourni les images du jeu), vous devez vous créer un compte: https://account.mapbox.com/auth/signin/?route-to=%22https%3A%2F%2Faccount.mapbox.com%2F%22
2. Quand vous êtes sur votre compte, copier le token publique par défaut de map box: 
3. ![image-20220501200038780](C:\Users\mikde\AppData\Roaming\Typora\typora-user-images\image-20220501200038780.png)
4. Ensuite, créer une variable d'environnement sur votre poste et mettez ce token comme valeur:
5. ![image-20220501200254826](C:\Users\mikde\AppData\Roaming\Typora\typora-user-images\image-20220501200254826.png)
6. Dans la classe Jeu.cs, mettez le nom de la clé dans le constructeur de ApiEndroit:![image-20220501200453649](C:\Users\mikde\AppData\Roaming\Typora\typora-user-images\image-20220501200453649.png)
6. Rédemarrez votre PC.

## Comment fonctionne le Bot?

1. Tout d'abord, il faut lancer le programme pour pouvoir l'utiliser:

   ![image-20220303183026345](C:\Users\mikde\AppData\Roaming\Typora\typora-user-images\image-20220303183026345.png)

2. Vous devez être dans le "Serveur de Mikael" pour pouvoir exécuter les commandes, car l'id du serveur est le mien. Si vous voulez utilisez le votre changez le guildId dans la classe Jeu.cs

3. En rentrant une commande "/" dans le discord, tous les commandes apparaitront.

![image-20220303183229137](C:\Users\mikde\AppData\Roaming\Typora\typora-user-images\image-20220303183229137.png)

## Commande

Pour utilisez la commande /loca-meteo, il faut passé en paramètre le nom de la ville et les initiales du pays. Ex: /loca-meteo Drummondville ca
	  /loca-meteo los angeles us



*Les autres commandes sont facilement utilisable.

## Jouer au jeu loca-guess

1. Vous devez utiliser la commande /loca-start pour démarrer le jeu. ** La commande "loca-endroit" ne pourra pas être utilisée tant et aussi lontemps que "loca-start" n'est pas exécuté.
2. Ensuite, le jeu se déroule en 5 étapes: vous répondez toujours avec la commande"/loca-endroit nomEndroit" Exemple: /loca-endroit mexique
3. Chaque bonne réponse vous s'apporte 30 points
4. Pour redémarrer le jeu re-faire /loca-start

## À savoir!

Le bot mets un peu de temps à démarrer et je n'ai pas trouvé la source du problème. Il faut attendre le "Ready!" dans la console avant de l'utiliser.
De plus, le jeu crash parfois ... :/

## Fait

- les commandes concernant l'aide utilisateur (sauf que les descriptions ne sont pas complétées)
- la météo
- Le jeu est fonctionnel 

## Pas fait

- Le système de pointage pour le jeu
- Le système de temps avec les réponse
- Le jeu n'a pas beaucoup de choix d'endroit
- Mettre le service d'intégration en continu sur gitlab
- Les Tests

## License

Voir la license MIT mis à la racine du projet.

