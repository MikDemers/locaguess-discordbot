﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Net;
using Discord.WebSocket;
using LocaGuess.Models;
using LocaGuess.Services;
using Discord.Addons.Interactive;
using Discord.Commands;

namespace LocaGuess.Controlleurs
{
    /// <summary>
    /// Nom: Mikael Demers
    /// Date: 2022-03-03
    /// Description: Cette classe gère la logique de l'endroit
    /// </summary>
    public class ControlleurEndroit : InteractiveBase
    {
        #region Attributs
        List<Endroit> _endroits;
        List<string> _choixPays;
        private readonly ApiEndroit _apiEndroit;
        private readonly DiscordSocketClient _client;
        private readonly ulong _guildId;
        private SocketGuild? _serveur;
        /// <summary>
        /// nous aide à mettre en place le jeu
        /// </summary>
        private JeuHelper _jeuHelper;
        /// <summary>
        /// représente les étapes du jeu ( 1 à 5)
        /// </summary>
        private int _etapeJeu;
        #endregion
        #region Constructeurs
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Constructeur de ControlleurEndroit
        /// </summary>
        /// <param name="apiEndroit">l'apiEndroit</param>
        /// <param name="client">le client discord</param>
        /// <param name="guildId">le id du serveur</param>
        public ControlleurEndroit(ApiEndroit apiEndroit, DiscordSocketClient client, ulong guildId)
        {
            _apiEndroit = apiEndroit;
            _client = client;
            _guildId = guildId;
            _serveur = null;
            _endroits = new List<Endroit>();
            _jeuHelper = new JeuHelper();
            _choixPays = new List<string>();
            _etapeJeu = 0;
        }
        #endregion
        #region Méthodes
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Permet de rendre la commande disponnible dans le discord "/loca-start" -> démarre le jeu.
        /// </summary>
        public async Task DemarrerJeuCommande_Pret()
        {
            _serveur = _client.GetGuild(_guildId);

            var serveurCommand = new SlashCommandBuilder()
                .WithName("loca-start")
                .WithDescription("Démarre le jeu locaguess");

            try
            {
                await _serveur.CreateApplicationCommandAsync(serveurCommand.Build());
            }
            catch (ApplicationCommandException exception)
            {
                Console.WriteLine(exception.ToString());
                throw;
            }
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Permet de rendre la commande disponnible dans le discord "/loca-endroit 'endroit'" 
        /// </summary>
        /// <returns></returns>
        public async Task RepondreJeuCommande_Pret()
        {
            _serveur = _client.GetGuild(_guildId);

            var serveurCommand = new SlashCommandBuilder()
                .WithName("loca-endroit")
                .WithDescription("Entrez votre réponse")
                .AddOption("reponse", ApplicationCommandOptionType.String, "entrez votre reponse", isRequired: true);


            try
            {
                await _serveur.CreateApplicationCommandAsync(serveurCommand.Build());
            }
            catch (ApplicationCommandException exception)
            {
                Console.WriteLine(exception.ToString());
                throw;
            }
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Cette méthode mets le jeu en place et affiche le début du jeu.
        /// </summary>
        /// <param name="commande">commande</param>
        /// <returns></returns>
        public async Task<string> JeuCommandeDiscordHandler(SocketSlashCommand commande)
        {
            _endroits.Clear();
            _etapeJeu = 1;
            _endroits = _jeuHelper.GenererEndroit();
            SetUrlEndroit(_endroits);

            MelangerPays();

            // pas utile, mais je garde pour l'instant
            // On fait les requêtes pour aller cherche l'image des endroits
            /*for (int i = 0; i < _endroits.Count; i++)
            {
                _endroits[i].ImageEndroit = _apiEndroit.GetImageAsync(_endroits[i]).Result;
            }*/

            await commande.RespondAsync(embed: AfficherEndroit().Build(), ephemeral: true);


            return _endroits[0].Pays;
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Cette méthode gère la réponse de l'usager si il à fait /loca-start avant.
        /// </summary>
        /// <param name="commande"></param>
        /// <returns>Vrai si la réponse est la même</returns>
        public async Task<bool> RepondreJeuCommandeDiscordHandler(SocketSlashCommand commande)
        {
            string reponseUsager = commande.Data.Options.First().Value.ToString();

            // Vérifie si l'usager à fait la commande /loca-start avant d'utiliser celle-ci.
            if (_endroits.Count == 5 && _etapeJeu < 6)
            {
                if (reponseUsager.ToLower() == _endroits[_etapeJeu - 1].Pays.ToLower())
                {
                    var embedBuild = new EmbedBuilder()
                        .WithTitle("Bravo!")
                        .WithDescription("Vous avez la bonne réponse!\n30 points vous serons ajouté!!")
                        .WithColor(Color.Green);

                    await commande.RespondAsync(embed: embedBuild.Build(), ephemeral: true);

                    // on incrément étape jeu quand l'usager à répondu
                    _etapeJeu++;

                    // On mélange les pays et on affiche la prochaine étape
                    MelangerPays();

                    await commande.FollowupAsync(embed: AfficherEndroit().Build(), ephemeral: true);

                    return true;
                }
                else
                {
                    var embedBuild = new EmbedBuilder()
                                        .WithTitle("Échec!")
                                        .WithDescription("Vous avez la mauvaise Réponse!\n0 points vous serons ajouté :/\n"
                                                        + $"La bonne réponse était {_endroits[0].Pays}")
                                        .WithColor(Color.Red);
                    await commande.RespondAsync(embed: embedBuild.Build(), ephemeral: true);

                    // on incrément étape jeu quand l'usager à répondu
                    _etapeJeu++;

                    // On mélange les pays et on affiche la prochaine étape
                    MelangerPays();

                    await commande.FollowupAsync(embed: AfficherEndroit().Build(), ephemeral: true);

                    return false;
                }
            }
            else
            {
                var embedBuild = new EmbedBuilder()
                                       .WithTitle("Veuillez faire /loca-start avant!")
                                       .WithDescription("Vous devez faire la commande /loca-start pour pouvoir jouer au jeu!")
                                       .WithColor(Color.Red);
                await commande.RespondAsync(embed: embedBuild.Build(), ephemeral: true);
                return false;
            }


        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Assigne les urls au modèle.
        /// </summary>
        /// <param name="endroits">Les endroits</param>
        public void SetUrlEndroit(List<Endroit> endroits)
        {
            for (int i = 0; i < _endroits.Count; i++)
            {
                _endroits[i].UrlEndroit = _apiEndroit.GetUrlApiEndroit(_endroits[i]);
            }
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Affiche les choix de pays et l'image du bon pays.
        /// </summary>
        /// <returns></returns>
        private EmbedBuilder AfficherEndroit()
        {
            return new EmbedBuilder()
                .WithTitle("Dans quel pays ce trouve cette endroit?")
                .WithDescription("1) " + _choixPays[0] +
                                 "\n2) " + _choixPays[1] +
                                 "\n3) " + _choixPays[2] +
                                 "\n4) " + _choixPays[3] +
                                 "\n5) " + _choixPays[4] +
                                 "\n6) " + _choixPays[5] +
                                 "\n7) " + _choixPays[6] +
                                 "\n8) " + _choixPays[7] +
                                 "\n9) " + _choixPays[8])
                .WithColor(Color.Teal)
                .WithImageUrl($"{_endroits[_etapeJeu - 1].UrlEndroit}");  
        }

        public void MelangerPays()
        {
            _choixPays = _jeuHelper.GenererChoixPays();

            _choixPays = _jeuHelper.MelangerListe(_endroits[_etapeJeu - 1], _choixPays);
        }
        #endregion





    }
}
