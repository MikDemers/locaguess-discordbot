﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LocaGuess.Services;
using LocaGuess.Models;
using System.Drawing;
using Discord.WebSocket;
using Discord;
using Discord.Net;

namespace LocaGuess.Controlleurs
{
    /// <summary>
    /// Nom: Mikael Demers
    /// Date: 2022-03-03
    /// Description: Cette classe gère la logique de la météo.
    /// </summary>
    public class ControlleurMeteo
    {
        private readonly ApiMeteo _apiMeteo;
        private readonly DiscordSocketClient _client;
        private readonly ulong _guildId;
        private SocketGuild? _serveur;
        private string _ville;
        private string _pays;
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-2
        /// Description: Constructeur de ControlleurMeteo.
        /// </summary>
        /// <param name="apiMeteo">L'apiMeteo</param>
        /// <param name="client">le client discord</param>
        /// <param name="guildId">le id du serveur</param>
        public ControlleurMeteo(ApiMeteo apiMeteo, DiscordSocketClient client, ulong guildId)
        {
            _apiMeteo = apiMeteo;
            _client = client;
            _guildId = guildId;
            _serveur = null;
            _ville = null;
            _pays = null;
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-02
        /// Description: Va chercher les informations sur la météo et garde que les informations nécessaireé
        /// </summary>
        /// <param name="ville">la ville</param>
        /// <param name="pays">les initiales du pays</param>
        /// <returns>une météo</returns>
        public Meteo RequeteMeteo (string ville, string pays)
        {
            MeteoJson meteoJson = _apiMeteo.GetMeteoAsync(ville, pays).Result;
            List<MeteoJson.Weather> weathers = meteoJson.weather;
            string? iconMeteo = null;
            if(weathers.Count > 0)
            {
                iconMeteo = weathers[0].icon;
            }
            return new Meteo(meteoJson.main.temp, meteoJson.main.humidity,
                ville, iconMeteo, meteoJson.main.feels_like, ConvertStringToBitMap(iconMeteo));
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-02
        /// Description: convertie la chaine de l'icone en image.
        /// </summary>
        /// <param name="iconMeteo">la chaine de caractère de l'icone</param>
        private Bitmap ConvertStringToBitMap(string iconMeteo)
        {
            return _apiMeteo.GetIconeMeteoAsync(iconMeteo).Result;
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-02
        /// Description: Permet de rendre la commande disponnible dans discord /loca-meteo "ville" "initial pays"
        /// </summary>
        public async Task MeteoCommandeDiscord_Pret()
        {
            _serveur = _client.GetGuild(_guildId);

            var serveurCommand = new SlashCommandBuilder()
                    .WithName("loca-meteo")
                    .WithDescription("Affiche la météo d'une ville selon vos choix")
                    .AddOption("ville", ApplicationCommandOptionType.String, "Entrez le nom de la ville", isRequired: true)
                    .AddOption("pays", ApplicationCommandOptionType.String, "Entrez les initiales du pays", isRequired: true);
            try
            {
                //await _client.CreateGlobalApplicationCommandAsync(commandeGlobale.Build());
                await _serveur.CreateApplicationCommandAsync(serveurCommand.Build());
            }
            catch (ApplicationCommandException exception)
            {
                Console.WriteLine(exception.ToString());
                throw;
            }
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-03
        /// Description: cette méthode retourne une réponse à la commande /loca-meteo avec les informations de la météo de la ville entrée.
        /// </summary>
        /// <param name="commande">commande</param>
        public async Task MeteoCommandeDiscordHandler(SocketSlashCommand commande)
        {
            var guildData = commande.Data.Options.Count;

            // Va chercher les valeurs entrées par l'utilisateur.
            string sVille = commande.Data.Options.ElementAt(0).Value.ToString();
            string sPays = commande.Data.Options.ElementAt(1).Value.ToString();

            Meteo meteoInfo = RequeteMeteo(sVille, sPays);

            var embedBuild = new EmbedBuilder()
                .WithTitle($"Voilà la météo pour {sVille}")
                .WithColor(Discord.Color.Orange)
                .WithDescription($"Temperature: {ConvertKelvinToCelsius(meteoInfo.Temperature)} °C \n" +
                                 $"Humidité: {meteoInfo.Humidite}% \n" +
                                 $"Temperature Ressenti: {ConvertKelvinToCelsius(meteoInfo.TemperatureRessenti)} °C")
                .WithThumbnailUrl($"https://openweathermap.org/img/wn/{meteoInfo.IconeMeteo}@2x.png");

            await commande.RespondAsync(embed: embedBuild.Build(), ephemeral: true);

        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-03
        /// Description: Convertie les kelvin en degrée celcius
        /// </summary>
        /// <param name="temperature">température en kelvin</param>
        /// <returns>température en int</returns>
        private int ConvertKelvinToCelsius (double temperature)
        {
            return ((int)(temperature - 273.15));
        }

    }
    
}
