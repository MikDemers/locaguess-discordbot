﻿using Discord;
using Discord.Net;
using Discord.WebSocket;
using System;
using System.Linq;
using System.Threading.Tasks;
using LocaGuess.Services;
using LocaGuess.Controlleurs;
using LocaGuess.Models;
namespace LocaGuess
{
    /// <summary>
    /// Nom: Mikael Demers
    /// Date: 2022-03-03
    /// Description: Le point d'entré du bot discord pour effectuer les actions.
    /// </summary>
    class Jeu
    {
        private DiscordSocketClient _client;
        /// <summary>
        /// Id du serveur discord
        /// </summary>
        private ulong _guildId = 935977356076646430;
        public static Task Main(string[] args) => new Jeu().MainAsync();

        private AideUtilisateur _aideUtilisateur;

        private ApiMeteo _apiMeteo;

        private ApiEndroit _apiEndroit;

        private ControlleurMeteo _controlleurMeteo;

        private ControlleurEndroit _controlleurEndroit;

        /// <summary>
        /// Les points totaux de l'utilisateur.
        /// </summary>
        private int _pointageUsager = 0;

        /// <summary>
        /// La réponse de l'utilisateur.
        /// </summary>
        private string _reponse = "";

        /// <summary>
        /// Attribut qui regarde si la réponse de l'usager est bonne ou non.
        /// </summary>
        private bool _estBonne = false;

        public async Task MainAsync()
        {
            // initialisation des attributs
            _client = new DiscordSocketClient();
            _apiMeteo = new ApiMeteo(Environment.GetEnvironmentVariable("MeteoApiKey"));
            _apiEndroit = new ApiEndroit(Environment.GetEnvironmentVariable("EndroitApiKey"));
            _controlleurMeteo = new ControlleurMeteo(_apiMeteo, _client, _guildId);
            _controlleurEndroit = new ControlleurEndroit(_apiEndroit, _client, _guildId);
            _aideUtilisateur = new AideUtilisateur(_client,_guildId);


            // Activation des commandes qui seront affichées sur discord
            _client.Log += Log;
            _client.Ready += _aideUtilisateur.AideCommandeDiscord_Pret;
            _client.Ready += _aideUtilisateur.CommandeFonctionnementJeu_Prêt;
            _client.Ready += _aideUtilisateur.CommandeCreateur_Prêt;
            _client.Ready += _controlleurMeteo.MeteoCommandeDiscord_Pret;
            _client.Ready += _controlleurEndroit.DemarrerJeuCommande_Pret;
            _client.Ready += _controlleurEndroit.RepondreJeuCommande_Pret;
            
            // Gère tous les réponses des commandes
            _client.SlashCommandExecuted += SlashCommandHandler;

            //  You can assign your bot token to a string, and pass that in to connect.
            //  This is, however, insecure, particularly if you plan to have your code hosted in a public repository.
            var token = Environment.GetEnvironmentVariable("DiscordBotToken");


            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();



            // Block this task until the program is closed.
            await Task.Delay(-1);
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-02-02
        /// Descrition: Sert simplement à afficher les logs.
        /// </summary>
        /// <param name="msg">le message</param>
        /// <returns></returns>
        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        /// <summary>
        /// Nom : Mikael Demers
        /// Date: 2022-02-03
        /// Description: Gère les réponses selon les commandes entrées dans discord.
        /// </summary>
        /// <param name="commande">la commande</param>
        /// <returns></returns>
        private async Task SlashCommandHandler(SocketSlashCommand commande)
        {
            switch (commande.Data.Name)
            {
                case "loca-aide":
                    await _aideUtilisateur.AideCommandeDiscordHandler(commande);
                    break;
                case "loca-jeu":
                    await _aideUtilisateur.CommandeFonctionnementJeuHandler(commande);
                    break;
                case "loca-createur":
                    await _aideUtilisateur.CommandeCreateurHandler(commande);
                    break;
                case "loca-meteo":
                    await _controlleurMeteo.MeteoCommandeDiscordHandler(commande);
                    break;
                case "loca-start":
                    _pointageUsager = 0;
                    _reponse = await _controlleurEndroit.JeuCommandeDiscordHandler(commande);
                    break;
                case "loca-endroit":
                    _estBonne = await _controlleurEndroit.RepondreJeuCommandeDiscordHandler(commande);

                    if (_estBonne)
                    {
                        _pointageUsager += 30;
                    }

                    break;
            }
        }
    }
}
