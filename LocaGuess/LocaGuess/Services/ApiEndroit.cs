﻿using LocaGuess.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LocaGuess.Services
{
    /// <summary>
    /// Nom: Mikael Demers
    /// Date: 2022-03-02
    /// Description: Cette classe sert à effectuer des requêtes vers un api.
    /// </summary>
    public class ApiEndroit : Api
    {
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: constructeur par défaut de la classe ApiMétéo
        /// </summary>
        /// <param name="apiKey"></param>
        public ApiEndroit(string apiKey) : base (apiKey)
        {

        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Fait une requête pour aller chercher l'image de l'endroit
        /// </summary>
        /// <param name="endroit">Endroit que l'on veut l'image</param>
        /// <returns>image</returns>
        public async Task<Bitmap> GetImageAsync(Endroit endroit)
        {
            HttpResponseMessage reponse = await _httpClient
                .GetAsync($"{endroit.UrlEndroit}");
            
            if (reponse.IsSuccessStatusCode)
            {
                var contenu = await reponse.Content.ReadAsStreamAsync();
                return new Bitmap(contenu);
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Permet d'envoyer l'url de l'api avec les informations de l'endroit.
        /// </summary>
        /// <param name="endroit">L'endroit</param>
        /// <returns>L'url</returns>
        public string GetUrlApiEndroit(Endroit endroit)
        {
            return $"https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/{endroit.Longitude},{endroit.Latitude},{endroit.Zoom},{endroit.Inclinaison},{endroit.Pitch}/{endroit.Largeur}x{endroit.Hauteur}?access_token={_apiKey}";
        }
    }
}
