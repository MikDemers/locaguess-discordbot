﻿using LocaGuess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocaGuess.Services
{
    /// <summary>
    /// Nom: Mikael Demers
    /// Date: 2022-05-01
    /// Description: Cette classe aide à mettre en place le jeu. Elle va permettre de choisir aléatoirement les endroits
    ///              à afficher.
    /// </summary>
    public class JeuHelper
    {
        /// <summary>
        /// Pour généré un nombre aléatoire.
        /// </summary>
        private Random _rnd;
        /// <summary>
        /// Tableau de chiffre généré aléatoirement.
        /// </summary>
        private int[] _chiffreAleatoire;
        /// <summary>
        /// Contient des endroits
        /// </summary>
        private List<Endroit> _endroits;

        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Constructeur de jeu helper.
        /// </summary>
        public JeuHelper()
        {
            _rnd = new Random();
            _chiffreAleatoire = new int[5];
            _endroits = new List<Endroit>();
        }

        // numéro, nomVille, pays, zoom, bearing, pitch, lon, lat
        /// <summary>
        /// Tableau qui contient des endroits de base pour le jeu.
        /// </summary>
        private string[,] _endroitBase = new string[10, 8]
        {
            {"1", "Drummondville", "Canada", "11.40", "0.00", "4.50", "-72.467010", "45.891014" },
            { "2", "Bierbeek", "Belgique", "11.40", "0.00", "4.50", "4.757019", "50.829113"},
            { "3", "Ashigaka", "Japon", "11.40", "0.00", "4.50", "139.465465", "36.297850"},
            { "4", "Seoul", "Corée du sud", "11.40", "0.00", "4.50", "126.974139", "37.565002"},
            { "5", "Palangka Raya", "Indonesie", "8.40", "-113.80", "4.50", "113.990293", "-2.251767"},
            { "6", "Querétako", "Mexique", "8.67", " 0.00", "4.50", "-100.023101", "20.618106"},
            { "7", "kuopio", "Finlande", "9.60", " 0.00", "4.50", "27.732061", "62.909484"},
            { "8", "Rajasthan", "Inde", "8.80", " 0.00", "4.50", "73.605115", "27.037214"},
            { "9", "Sana'a", "Yemen", "8.27", " 0.00", "4.50", "44.870199", "15.464263"},
            { "10", "Ankara", "Turky", "8.54", " 0.00", "4.50", "32.924842", "39.904129"}
        };

        /// <summary>
        /// Tableau qui contient des pays de base
        /// </summary>
        private string[] _paysBase = new string[20]
        {
            "Cameroun", "Australie", "Argentine", "Iran", "Jordanie",
            "Monaco", "Portugal", "Tchad", "Allemagne", "Russie", "Pays-Bas",
            "Pérou", "France", "Chine", "Norvège", "Maroc", "Malaisie",
            "Liban", "Hongrie", "Gambie"
        };

        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Génère 5 nombre alétoire.
        /// </summary>
        private void GenererCinqNombresDifferents()
        {
            for (int i = 0; i < 5; i++)
            {
                int iNombre = _rnd.Next(1, 11);

                _chiffreAleatoire[i] = iNombre;
            }
        }

        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Génère 5 endroits à mettre dans la liste
        /// </summary>
        /// <returns>liste avec 5 endroits</returns>
        public List<Endroit> GenererEndroit()
        {
            _endroits.Clear();
            GenererCinqNombresDifferents();
            
            for (int pos = 0; pos < _chiffreAleatoire.Length; pos++)
            {
                Endroit endroit = new Endroit()
                {
                    Ville = _endroitBase[_chiffreAleatoire[pos] - 1, 1],
                    Pays = _endroitBase[_chiffreAleatoire[pos] - 1, 2],
                    Zoom = float.Parse(_endroitBase[_chiffreAleatoire[pos] - 1, 3]),
                    Inclinaison = float.Parse(_endroitBase[_chiffreAleatoire[pos] - 1, 4]),
                    Pitch = float.Parse(_endroitBase[_chiffreAleatoire[pos] - 1, 5]),
                    Longitude = float.Parse(_endroitBase[_chiffreAleatoire[pos] - 1, 6]),
                    Latitude = float.Parse(_endroitBase[_chiffreAleatoire[pos] - 1, 7]),
                    Hauteur = 400,
                    Largeur = 400,
                    ImageEndroit = null,
                    UrlEndroit = null
                };
                _endroits.Add(endroit);
            }
            return _endroits;
        }

        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Genère 8 choix de pays différents pour le jeu. 
        /// </summary>
        /// <returns>Liste de 8 pays</returns>
        public List<string> GenererChoixPays()
        {
            List<string> sListePays = new List<string>();
            int iChoix = 0;
            while (sListePays.Count != 8)
            {
                iChoix = _rnd.Next(0, 20);
                if (!sListePays.Contains(_paysBase[iChoix]))
                {
                    sListePays.Add(_paysBase[iChoix]);
                }
                
            }

            return sListePays;
        }

        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-05-01
        /// Description: Melange l'endroit choisit avec les 8 pays dans une liste pour que les choix soit place aléatoirement.
        /// </summary>
        /// <param name="endroit">Endroit</param>
        /// <param name="pays">la liste de pays</param>
        /// <returns></returns>
        public List<string> MelangerListe (Endroit endroit, List<string> pays)
        {
            pays.Add(endroit.Pays);

            return pays.OrderBy(p => _rnd.Next()).ToList();
        }
    }
}
