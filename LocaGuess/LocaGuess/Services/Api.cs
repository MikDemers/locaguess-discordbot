﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace LocaGuess.Services
{
    public abstract class Api
    {
        /// <summary>
        /// La clé de l'api Open Weather Map.
        /// </summary>
        protected readonly string _apiKey;
        /// <summary>
        /// le http client pour faire les requêtes.
        /// </summary>
        protected readonly HttpClient _httpClient;
        
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-03
        /// Description: constructeur de l'api
        /// </summary>
        /// <param name="apiKey">la clé de l'api</param>
        public Api(string apiKey)
        {
            _apiKey = apiKey;
            _httpClient = new HttpClient();
        }
    }
}
