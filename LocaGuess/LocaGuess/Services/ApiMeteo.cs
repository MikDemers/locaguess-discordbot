﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LocaGuess.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Text.Json;
using System.Drawing;

namespace LocaGuess.Services
{
    /// <summary>
    /// Nom: Mikael Demers
    /// Date: 2022-03-02
    /// Description: Cette classe sert à effectuer des requêtes vers un api.
    /// </summary>
    public class ApiMeteo : Api
    {
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-02
        /// Description: constructeur par défaut de la classe ApiMétéo
        /// </summary>
        /// <param name="apiKey"></param>
        public ApiMeteo(string apiKey) : base(apiKey)
        {
            
        }


        /// <summary>
        /// Nom: Mikael Demers
        /// Date; 2022-03-02
        /// Description: Permet d'aller chercher les informations de la météo selon la ville et le pays choisit par l'utilisateur.
        /// </summary>
        /// <param name="ville">la ville</param>
        /// <param name="pays">le pays</param>
        /// <returns>Les informations de la météo en Json</returns>
        public async Task<MeteoJson> GetMeteoAsync(string ville, string pays)
        {
            HttpResponseMessage reponse = await _httpClient.GetAsync($"https://api.openweathermap.org/data/2.5/weather?q={ville},{pays}&APPID={_apiKey}");
            if (reponse.IsSuccessStatusCode)
            {
                var contenuRequête = await reponse.Content.ReadAsStringAsync();

                return JsonSerializer.Deserialize<MeteoJson>(contenuRequête);
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-02
        /// Description: Cette classe permet d'aller chercher l'image de la meteo sur internet.
        /// </summary>
        /// <param name="iconeMeteo">le string de l'icone meteo</param>
        /// <returns> l'image en bitmap</returns>
        public async Task<Bitmap> GetIconeMeteoAsync(string iconeMeteo)
        {
            HttpResponseMessage reponse = await _httpClient.GetAsync($"https://openweathermap.org/img/wn/{iconeMeteo}@2x.png");
            if (reponse.IsSuccessStatusCode)
            {
                var contenu = await reponse.Content.ReadAsStreamAsync();
                return new Bitmap(contenu);
            }else
            {
                return null;
            }

        }
    }
}
