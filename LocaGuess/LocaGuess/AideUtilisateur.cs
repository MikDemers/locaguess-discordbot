﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Net;
using Discord.WebSocket;

namespace LocaGuess
{
    /// <summary>
    /// Nom: Mikael Demers
    /// Date: 2022-02-16
    /// Description: Cette classe sert à informer l'utilisateur sur les
    ///              commandes qu'il peut utiliser avec une brève description.
    /// </summary>
    public class AideUtilisateur
    {
        private readonly DiscordSocketClient _client;
        private readonly ulong _guildId;
        private SocketGuild? _serveur;
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-02-17
        /// Description: Constructeur de l'aide utilisateur
        /// </summary>
        /// <param name="client">Le discord socket sur programme</param>
        public AideUtilisateur(DiscordSocketClient client, ulong guildId)
        {
            _client = client;
            _guildId = guildId;
            _serveur = null;
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-02-17
        /// Description: Cette méthode crée la commande /loca-help dans discord.
        /// </summary>
        public async Task AideCommandeDiscord_Pret()
        {
            _serveur = _client.GetGuild(_guildId);

            // La commandes pour afficher tous les commandes du discord
            var serveurCommand = new SlashCommandBuilder()
                    .WithName("loca-help")
                    .WithDescription("Commande test pour création de commande avec guildId");


            try
            {
                //await _client.CreateGlobalApplicationCommandAsync(commandeGlobale.Build());
                await _serveur.CreateApplicationCommandAsync(serveurCommand.Build());
            }
            catch (ApplicationCommandException exception)
            {
                Console.WriteLine(exception.ToString());
                throw;
            }
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-02-23
        /// Description: Cette méthode retourne la réponse de la commande /loca-help.
        ///              Elle envoi tous les informations à connaitre sur les commandes du bot.
        /// </summary>
        /// <param name="commande">la commande</param>
        public async Task AideCommandeDiscordHandler(SocketSlashCommand commande)
        {
            var fieldBuilder = new EmbedFieldBuilder().WithName("/loca-createur").WithValue("Affiche le créateur/fondateur du bot.");
            var meteoBuilder = new EmbedFieldBuilder().WithName("/loca-meteo 'ville' 'pays'").WithValue("Affiche la météo de l'endroit que vous avez choisit.");
            var fieldTest = new EmbedFieldBuilder().WithName("/loca-aide").WithValue("Commande pour aider les utilisateurs");
            var jeuBuilder = new EmbedFieldBuilder().WithName("/loca-start").WithValue("Démarre le jeu de LocaGuess");
            var explicationJeuBuilder = new EmbedFieldBuilder().WithName("/loca-jeu").WithValue("Explique comment jouer au jeu");
            var endroitBuilder = new EmbedFieldBuilder().WithName("/loca-endroit 'reponse' ").WithValue("Sert à entrer le pays dans les choix de réponse");


            var embedBuild = new EmbedBuilder()
                .WithTitle("Les commandes:")
                .WithColor(Color.Teal)
                .WithFields(fieldBuilder)
                .WithFields(fieldTest)
                .WithFields(meteoBuilder)
                .WithFields(jeuBuilder)
                .WithFields(explicationJeuBuilder)
                .WithFields(endroitBuilder)
                .WithTimestamp(DateTimeOffset.Now);

            await commande.RespondAsync(embed: embedBuild.Build(), ephemeral: true);
        }

        public async Task CommandeFonctionnementJeu_Prêt()
        {
            _serveur = _client.GetGuild(_guildId);
            // La commande pour voir le fonctionnement du jeu
            var commandeFonctionnementJeu = new SlashCommandBuilder()
                            .WithName("loca-jeu")
                            .WithDescription("Explication du jeu Geoguessr");

           

            try
            {
                await _serveur.CreateApplicationCommandAsync(commandeFonctionnementJeu.Build());
            }
            catch (ApplicationCommandException exception)
            {
                Console.WriteLine(exception.ToString());
                throw;
            }
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-02-23
        /// Description: cette méthode retourne une réponse à la commande /loca-jeu avec le fonctionnement du jeu.
        /// </summary>
        /// <param name="commande">la commande</param>
        public async Task CommandeFonctionnementJeuHandler(SocketSlashCommand commande)
        {
            var embedBuild = new EmbedBuilder()
                .WithTitle("Explication du jeu:")
                .WithDescription("Comment lancer le jeu?\n" +
                                 "La commande /loca-start démarre le jeu\n" +
                                 "Quand le jeu est commencer, un endroit vous sera affiché et vous devez la commande /loca-endroit {réponse} " +
                                 "pour donner votre réponse\n")
                .WithColor(Color.LighterGrey)
                .WithCurrentTimestamp()
                .WithThumbnailUrl("https://www.pngkey.com/png/detail/852-8521906_google-maps-icon-png-point-de-google-maps.png");

            await commande.RespondAsync(embed: embedBuild.Build(), ephemeral: true);
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-02-23
        /// Description: Cette crée la commande /loca-createur dans le discord avec une description.
        /// </summary>
        public async Task CommandeCreateur_Prêt()
        {
            _serveur = _client.GetGuild(_guildId);

            var commandeCreateur = new SlashCommandBuilder()
                    .WithName("loca-createur")
                    .WithDescription("Affiche le Créateur/Fondateur du projet");

            try
            {
                await _serveur.CreateApplicationCommandAsync(commandeCreateur.Build());
            }
            catch (ApplicationCommandException exception)
            {
                Console.WriteLine(exception.ToString());
                throw;
            }
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-02-23
        /// Description: Cette méthode retourne la réponse de la commande /loca-createur avec une description du créateur.
        /// </summary>
        /// <param name="commande">la commande</param>
        public async Task CommandeCreateurHandler(SocketSlashCommand commande)
        {
            var embedBuild = new EmbedBuilder()
                .WithTitle("Créateur:")
                .WithDescription("Nom: Mikael Demers\n" +
                                 "Age: 23 ans\n" +
                                 "Ce projet a été initié lors en session hiver au Cégep de Drummondville dans le cours Développement native IV\n" +
                                 "Merci à tous ceux qui contribueront au projet!")
                .WithColor(Color.Blue)
                .WithCurrentTimestamp()
                .WithThumbnailUrl("https://rccfc.ca/wp-content/uploads/2019/08/Site-Web-Logo-Quebec-Drummonville.jpg");

            await commande.RespondAsync(embed: embedBuild.Build(), ephemeral: true);
        }
    }
}
