﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LocaGuess.Models
{
    /// <summary>
    /// Nom: Mikael Demers
    /// Date: 2022-02-02
    /// Description: Cette classe sert de modèle pour les informations importante de la météo.
    /// </summary>
    public class Meteo : Position
    {
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-02
        /// Description: Ce constructeur permet de créer une méto avec son image.
        /// </summary>
        /// <param name="temperature">la température</param>
        /// <param name="humidite">humidité</param>
        /// <param name="ville">la ville</param>
        /// <param name="iconeMeteo">la chaine de caractère représentant l'icone</param>
        /// <param name="temperatureRessenti">température ressenti dehors</param>
        /// <param name="imageIconeMeteo">l'image de la chaine de caractère de l'icone</param>
        public Meteo(double temperature, double humidite, string ville, string? iconeMeteo, double temperatureRessenti, Bitmap imageIconeMeteo)
        {
            Temperature = temperature;
            Humidite = humidite;
            Ville = ville;
            IconeMeteo = iconeMeteo;
            TemperatureRessenti = temperatureRessenti;
            ImageIconMeteo = imageIconeMeteo;
        }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-02
        /// Description: Ce constructeur permet de créer une météo par défaut avec les propriétés de base.
        /// </summary>
        /// <param name="temperature">la température</param>
        /// <param name="humidite">humidité</param>
        /// <param name="ville">la ville</param>
        /// <param name="iconeMeteo">la chaine de caractère représentant l'icone</param>
        /// <param name="temperatureRessenti">température ressenti dehors</param>
        public Meteo(double temperature, double humidite, string ville, string? iconeMeteo, double temperatureRessenti)
        {
            Temperature = temperature;
            Humidite = humidite;
            Ville = ville;
            IconeMeteo = iconeMeteo;
            TemperatureRessenti = temperatureRessenti;
            ImageIconMeteo = null;
        }

        public double Temperature { get; set; }
        public double Humidite { get; set; }
        public string Ville { get; set; }
        public string? IconeMeteo { get; set; }
        public Bitmap? ImageIconMeteo { get; set; }
        public double  TemperatureRessenti { get; set; }
        /// <summary>
        /// Nom: Mikael Demers
        /// Date: 2022-03-02
        /// Description: override de la méthode ToString pour afficher la météo.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (Temperature.ToString() + "\n" +
                    Humidite.ToString() + "\n" +
                    Ville + "\n" + IconeMeteo + "\n" + TemperatureRessenti.ToString());
        }
    }
}
