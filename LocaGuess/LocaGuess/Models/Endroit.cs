﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocaGuess.Models
{
    /// <summary>
    /// Nom: Mikael Demers
    /// Date: 2022-02-02
    /// Description: Cette classe sert de modèle pour les informations importante de l'endroit.
    /// </summary>
    public class Endroit : Position
    {
        public string Pays { get; set; }
        public string Ville { get; set; }
        public float Zoom { get; set; }
        public float Inclinaison { get; set; }
        public int Hauteur { get; set; }
        public int Largeur { get; set; }
        public float Pitch { get; set; }
        public Bitmap? ImageEndroit { get; set; }
        public string? UrlEndroit { get; set; }
    }
}
