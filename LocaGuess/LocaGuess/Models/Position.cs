﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocaGuess.Models
{
    /// <summary>
    /// Nom: Mikael Demers
    /// Date: 2022-03-03
    /// Description: Cette classe contient seulement la position sur les maps.
    /// </summary>
    public class Position
    {
        public float Longitude { get; set; }
        public float Latitude { get; set; }
    }
}
